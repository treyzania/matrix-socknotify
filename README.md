# matrix-socknotify

Have a UNIX socket that sends messages in a Matrix channel when you write to it.

## Running

Some important envvars:

* `MATRIX_SERVER`
* `MATRIX_USERNAME`
* `MATRIX_PASSWORD`

Command-line args:

```bash
./matsocknotify <channelid> <socketpath>
```

## Requirements

You should probably install the `socat` utility, it makes testing easier.
