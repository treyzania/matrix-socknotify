#!/usr/bin/env python3

import os
import socket
import sys

from matrix_client.client import MatrixClient

def main():
    server = os.getenv('MATRIX_SERVER')
    username = os.getenv('MATRIX_USERNAME')
    password = os.getenv('MATRIX_PASSWORD')

    chan_id = sys.argv[1]
    sock_path = sys.argv[2]

    # First connect to matrix.
    client = MatrixClient(server)
    client.login(username=username, password=password)
    print('Logged into', server)

    if not chan_id in client.rooms:
        print('I can\'t seem to find the room', chan_id)
        sys.exit(1)

    room = client.rooms[chan_id]

    # Next, create the socket.
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

    # Now bind!
    if os.path.exists(sock_path):
        try:
            os.unlink(sock_path)
        except OSError as e:
            if os.path.exists(sock_path):
                raise e

    sock.bind(sock_path)
    sock.listen(0)

    while True:
        conn, addr = sock.accept()
        body = conn.recv(4096) # FIXME Make this bigger
        if len(body) > 0:
            room.send_text(str(body, 'utf8'))
    
        conn.close()

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('improper arguments passed, check README.md')
        sys.exit(1)
    main()
